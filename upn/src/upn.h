/* 
 * Module      : ZHAW PROGC upn
 * Description : upn ->  RPN: Reverse Polish Notation
 * $Id: upn.h 2015-07-02 metl$
 */

#ifndef UPN_H_
#define UPN_H_

#include "../../calc/src/calc.h"
#include "../../stack/src/stack.h"
#include "../../grade/src/grade.h"

void calculate(char**);
float average(char*);

/* pointer to function address */
typedef float (*Fnct)(float, float);

/* upn operation by functional pointer */
float upn(float (*func)(float, float), float, float);

#endif /* UPN_H_ */