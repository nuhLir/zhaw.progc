/* 
 * Module      : ZHAW PROGC upn
 * Description : upn ->  RPN: Reverse Polish Notation
 * $Id: upn.c 2015-07-02 metl$
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "upn.h"

/* array of function addresses (mul, add, sub, ...) */
const Fnct operator[9] = { nop, nop, mul, add, nop, sub, nop, dif, pot };

/* 
 * calculate a term using a function pointer
 * params:
 *	*func  > pointer to function with
 *           - return: float
 *           - params: two float 
 *  float > 1st term variable
 *  float > 2nd term variable
 * return:
 *	float > result of term
 */ 
float upn(float (*func)(float x, float y), float z1, float z2) {
	return ((*func)(z1, z2));
}

/* 
 * calculate a term using rpn
 * - number: push to stack
 * - operator: pop two numbers, calculate and push result
 * - term must end with '\0' 
 * params:
 *	**term > rpn term in form "2" "3" "4" "+" "*"
 * return:
 *	float > result of term
 */ 
void calculate(char** term) {
	while (*term){
		int is_pot = **term == 94;
		int is_operator = 48 > **term || is_pot;

		if (is_operator) {
			int operator_index = **term - 40;

			if (is_pot) operator_index = 8;

			push(upn(operator[operator_index], pop(), pop()));
		}
		else {
			push(atof(*term));
		}
		term++;
	}
}

/* 
 * calculate average of grades using rpn (upn)
 * params:
 *	*grades > in form of "ABCD"
 * return:
 *	float > result
 */ 
float average(char *grades){
	if (!check_grades(grades)) return 1.0;
	int c = 0, d = 0;
	while(*grades) {
		push(GRADE_2_MARK(*grades));
		grades++;
		c++;
	}
	while (d++ != c) push(upn(add, pop(), pop()));
	push(c);
	return upn(dif, pop(), pop());
}
