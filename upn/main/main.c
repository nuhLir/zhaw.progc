#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../src/upn.h"

int main( int argc, char* argv[] ){
	if (argc < 4){
        printf( "Benutzung: %s <UPN Term>\n\nBeispiel:\n\t1 2 3 ^ +\n\t4 5 6 + - 3 +\n", argv[0]);
    }

	char** term = malloc((argc + 1) * sizeof(char *));

	for (int i = 0; i < argc - 1; i++) {
		char *current_argument = argv[i + 1];

		term[i] = malloc((strlen(current_argument) + 1) * sizeof(char));
		strcpy(term[i], current_argument);
	}

	// delimiter at the end
	term[argc - 1] = malloc(sizeof(char));
	term[argc - 1] = 0;

	printf("\n\n");

	// do the calculation
	calculate(term);

	printf("Resultat: %f\nm", pop());

	return EXIT_SUCCESS;
}