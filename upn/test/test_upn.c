/* 
 * Module      : ZHAW PROGC upn
 * Description : CUnit Tests > upn
 * $Id: test_upn.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h> 
#include <CUnit/Automated.h>
#include "upn.h"

void test_calculate_array(void);
void test_calculate_string(void);
void test_average(void);
void test_calculate_fractions(void);
void test_calculate_fractions_mixed(void);
void test_calculate_pot(void);
void test_calculate_div_zero(void);
void test_calculate_string_mixed(void);

int *test_upn(int *res, char *mode) {
	CU_pSuite suite;
	CU_initialize_registry();

	suite = CU_add_suite("upn", NULL, NULL);

	CU_ADD_TEST(suite, test_calculate_array);
	CU_ADD_TEST(suite, test_calculate_string);	
	CU_ADD_TEST(suite, test_average);		
	CU_ADD_TEST(suite, test_calculate_fractions);
	CU_ADD_TEST(suite, test_calculate_fractions_mixed);
	CU_ADD_TEST(suite, test_calculate_pot);
	CU_ADD_TEST(suite, test_calculate_div_zero);
	CU_ADD_TEST(suite, test_calculate_string_mixed);

	if (mode == NULL) {
		CU_basic_set_mode(CU_BRM_VERBOSE);
		CU_basic_run_tests();
	} else {
		CU_set_output_filename(mode);
		CU_automated_run_tests();
	} 
	
	if (res != NULL) {
		*(res + 0) = CU_get_number_of_tests_run();
		*(res + 1) = CU_get_number_of_asserts();
		*(res + 2) = CU_get_number_of_successes();
	}
	
	CU_cleanup_registry();
	return res;
}

void test_calculate_array(void) {
	char *term1[] = { "1", "2", "3", "+", "+", 0 };
	calculate(term1);
	CU_ASSERT_DOUBLE_EQUAL(6.0f, pop(), 0.01f);
	
	char *term2[] = { "1.5", "2.7", "3.1", "4.3", "5.5", "+", "-", "+", "-", 0 };
	calculate(term2);

	float result = pop();
	CU_ASSERT_DOUBLE_EQUAL(5.5f, result, 0.01f);
	
	char *term3[] = { "1", "2.72", "3.1", "4.3", "5.5", "*", "-", "/", "/", "0.5f", "/", 0 };
	calculate(term3);
	CU_ASSERT_DOUBLE_EQUAL(-15.11f, pop(), 0.01f);
	
	char *term4[] = { "10", "3", "/", 0 };
	calculate(term4);
	CU_ASSERT_DOUBLE_EQUAL(3.33f, pop(), 0.01f);
	
	char *term5[] = { "10", "3", "-", 0 };
	calculate(term5);
	CU_ASSERT_DOUBLE_EQUAL(7.0f, pop(), 0.01f);

	char *term6[] = { get_mark('A'), get_mark('B'), "+", get_mark('C'), "+", get_mark('D'), "+",0 };
	calculate(term6);
	CU_ASSERT_DOUBLE_EQUAL(18, pop(), 0.01f);
}

void test_calculate_string(void) {
	char *term1[] = { GRADE_2_STR('A'), GRADE_2_STR('B'), GRADE_2_STR('C'), "+", "+", 0 };
	calculate(term1);
	CU_ASSERT_DOUBLE_EQUAL(15.0f, pop(), 0.01f);
}

void test_average(void) {
	CU_ASSERT_DOUBLE_EQUAL(4.83f, average("AABBCD"), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(4.5f, average("ABCD"), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(4.5f, average("BCAD"), 0.01f);
}

void test_calculate_fractions(void){
	char *term1[] = { "1", "2", "/", 0 };
	calculate(term1);
	CU_ASSERT_DOUBLE_EQUAL(0.5f, pop(), 0.01f);

	char *term2[] = { "1", "4", "/", 0 };
	calculate(term2);
	CU_ASSERT_DOUBLE_EQUAL(0.25f, pop(), 0.01f);
}

void test_calculate_fractions_mixed(void){
	char *term1[] = { "1", "2", "/", "1", "+", 0 };
	calculate(term1);
	CU_ASSERT_DOUBLE_EQUAL(1.5f, pop(), 0.01f);

	char *term2[] = { "1", "4", "/", "2", "2", "^", "+", 0 };
	calculate(term2);
	CU_ASSERT_DOUBLE_EQUAL(4.25f, pop(), 0.01f);

	char *term3[] = { "1", "4", "/", "2", "2", "^", "+", "2", "+", 0 };
	calculate(term3);
	CU_ASSERT_DOUBLE_EQUAL(6.25f, pop(), 0.01f);
}

void test_calculate_pot(void){
	char *term1[] = { "2", "3", "2", "^", "+", 0 };
	calculate(term1);
	CU_ASSERT_DOUBLE_EQUAL(11.0f, pop(), 0.01f);

	char *term2[] = { "1", "2", "3", "^", "+", 0 };
	calculate(term2);
	CU_ASSERT_DOUBLE_EQUAL(9.0f, pop(), 0.01f);

	char *term3[] = { "4", "4", "4", "^", "-", "2", 0 };
	calculate(term3);
	CU_ASSERT_DOUBLE_EQUAL(2.0f, pop(), 0.01f);
}

void test_calculate_div_zero(void) {
	char *term1[] = { "2", "0", "/" , 0 };
	calculate(term1);
	CU_ASSERT_DOUBLE_EQUAL(-0.0f, pop(), 0.01f);

	char *term2[] = { "15", "0", "/" , 0 };
	calculate(term2);
	CU_ASSERT_DOUBLE_EQUAL(-0.0f, pop(), 0.01f);
}

void test_calculate_string_mixed(void) {
	char *term1[] = { GRADE_2_STR('A'), "4.5", "+", 0 };
	calculate(term1);
	CU_ASSERT_DOUBLE_EQUAL(10.5f, pop(), 0.01f);
}