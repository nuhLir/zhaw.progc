/* 
 * Module      : ZHAW PROGC student
 * Description : CUnit Tests > student
 * $Id: test_student.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h>
#include <CUnit/Automated.h>
#include "student.h"

/* redirect stdout to file */
#define STDOUT_FILE		freopen("test_stdout.txt", "w", stdout);
/* redirect stdout to console */
#ifdef __APPLE__
 #define STDOUT_CONSOLE	freopen("/dev/tty", "a", stdout);
#else 
 #define STDOUT_CONSOLE	freopen("CONOUT$", "w", stdout);
#endif

void test_new_student(void);
void test_swap_students(void);
void test_del_student(void);
void test_print_student(void);
void test_list_student(void);

int *test_student(int *res, char *mode) {
	CU_pSuite suite;
	CU_initialize_registry();
	
	suite = CU_add_suite("student", NULL, NULL);

	CU_ADD_TEST(suite, test_new_student);	
	CU_ADD_TEST(suite, test_swap_students);
	CU_ADD_TEST(suite, test_del_student);
	CU_ADD_TEST(suite, test_print_student);
	CU_ADD_TEST(suite, test_list_student);
	
	if (mode == NULL) {
		CU_basic_set_mode(CU_BRM_VERBOSE);
		CU_basic_run_tests();
	} else {
		CU_set_output_filename(mode);
		CU_automated_run_tests();
	} 
	
	if (res != NULL) {
		*(res + 0) = CU_get_number_of_tests_run();
		*(res + 1) = CU_get_number_of_asserts();
		*(res + 2) = CU_get_number_of_successes();
	}
	
	CU_cleanup_registry();
	return res;
}

void test_new_student(void) {
    Student *s;

	CU_ASSERT_PTR_NOT_NULL(s = new_student("Test", "ABC"));
	CU_ASSERT_TRUE(del_student(s));
    CU_ASSERT_PTR_NOT_NULL(s = new_student("Test", ""));
	CU_ASSERT_TRUE(del_student(s));
    CU_ASSERT_PTR_NOT_NULL(s = new_student("Test", NULL));
	CU_ASSERT_TRUE(del_student(s));
    CU_ASSERT_PTR_NULL(new_student("", "ABC"));
    CU_ASSERT_PTR_NULL(new_student(NULL, "ABC"));
    CU_ASSERT_PTR_NULL(new_student("", ""));
    CU_ASSERT_PTR_NULL(new_student(NULL, NULL));
}

void test_swap_students(void) {
    Student *s1, *s2;
	CU_ASSERT_PTR_NOT_NULL(s1 = new_student("Student1", "ABC"));
	if (NULL != s1) {
		CU_ASSERT_STRING_EQUAL("Student1",s1->name);
		CU_ASSERT_STRING_EQUAL("ABC",s1->grades);
		CU_ASSERT_DOUBLE_EQUAL(0.0f ,s1->mark, 0.01f);
	}
	CU_ASSERT_PTR_NOT_NULL(s2 = new_student("Student2", "CD"));
	CU_ASSERT_TRUE(swap_students(s1, s2));
	if (NULL != s1) {
		CU_ASSERT_STRING_EQUAL("Student2",s1->name);
		CU_ASSERT_STRING_EQUAL("CD",s1->grades);
		CU_ASSERT_DOUBLE_EQUAL(0.0f ,s1->mark, 0.01f);
	}
	if (NULL != s2) {
		CU_ASSERT_STRING_EQUAL("Student1",s2->name);
		CU_ASSERT_STRING_EQUAL("ABC",s2->grades);
		CU_ASSERT_DOUBLE_EQUAL(0.0f ,s2->mark, 0.01f);
	}
	CU_ASSERT_FALSE(swap_students(s1, NULL));
	CU_ASSERT_FALSE(swap_students(NULL, s2));
	CU_ASSERT_FALSE(swap_students(NULL, NULL));	
	CU_ASSERT_TRUE(del_student(s1));
	CU_ASSERT_TRUE(del_student(s2));	
}

void test_del_student(void) {
    Student *s;
	CU_ASSERT_PTR_NOT_NULL(s = new_student("Student1", "ABC"));
	CU_ASSERT_TRUE(del_student(s));
	CU_ASSERT_FALSE(del_student(NULL));
}

void test_print_student(void) {
    Student *s;
	CU_ASSERT_PTR_NOT_NULL(s = new_student("Student1", "ABC"));
	STDOUT_FILE
	// ON OSX, this is the PROPER VALUE
	CU_ASSERT(8 == print_student(s));
	STDOUT_CONSOLE
	CU_ASSERT_TRUE(del_student(s));	
}

void test_list_student(void) {
    Student *s;

	CU_ASSERT_PTR_NOT_NULL(s = new_student("Peter", "AAABC"));
	s->mark = 5.0f;
	CU_ASSERT_STRING_EQUAL("Peter-5.0-AAABC", list_student(s, "-"));
	CU_ASSERT_TRUE(del_student(s));

	CU_ASSERT_PTR_NOT_NULL(s = new_student("Kushtrim", NULL));
	CU_ASSERT_STRING_EQUAL("Kushtrim/0.0", list_student(s, "/"));
	CU_ASSERT_TRUE(del_student(s));

	CU_ASSERT_PTR_NOT_NULL(s = new_student("Refki Baca", "DDAACCAABBBBB"));
	CU_ASSERT_STRING_EQUAL("Refki BacaDELIMITER0.0DELIMITERDDAACCAABBBBB", list_student(s, "DELIMITER"));
	CU_ASSERT_TRUE(del_student(s));
}
