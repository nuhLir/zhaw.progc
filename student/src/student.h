/* 
 * Module      : ZHAW PROGC student
 * Description : utest > student
 * $Id: student.h 2015-07-02 metl$
 */
 
#ifndef STUDENT_H_
#define STUDENT_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define SIZE		256

#ifndef FALSE
#define FALSE 		0
#define TRUE 		!FALSE
#endif /* FALSE */

typedef struct {
    char *name;
	char *grades;
	float mark;
} Student;

Student *new_student(char *, char *);
int swap_students(Student * const, Student * const);
int del_student(Student *) ; 
int print_student(Student *);
char *list_student(Student *, const char * const);

#endif /* STUDENT_H_ */
