/* 
 * Module      : ZHAW PROGC student
 * Description : utest > student
 * $Id: student.c 2015-07-02 metl$
 */
 
#include "student.h"

 /* 
 * new Student 
 * allocate and initialize (Student, Name and Grades) and 
 * return the pointer to new Student struct
 * set grades to "" when Student has no grades
 * params:
 *	char * > name: student name
 *	char * > grades: student grades
 * return:
 *	Student * > pointer to new allocated Student struct or
 *              - NULL when name is NULL
 *              - NULL when length of name is 0
 *              - NULL when allocation of name fails
 *              - NULL when allocation of grades fails
 *              - NULL when allocation of Student struct fails
 */ 
Student *new_student(char *name, char *grades) {
	if (name == NULL) return NULL;
	if (strlen(name) == 0) return NULL;

	Student *student = (Student *)malloc(sizeof(Student));

	student->name = (char *)malloc(strlen(name) * sizeof(char));
	student->mark = 0.0f;
	student->grades = NULL;

	if (student == NULL) return NULL;
	if (student->name == NULL) {
		free(student->name);
		free(student);

		return NULL;
	}

	if (grades != NULL) {
		student->grades = (char *)malloc(strlen(grades) * sizeof(char));
		strcpy(student->grades, grades);
	}

	strcpy(student->name, name);

	return student;
}

 /* 
 * swap two Students 
 * params:
 *	Student * > s1: pointer to Student struct
 *	Student * > s2: pointer to Student struct
 * return:
 *	int > 0 (TRUE) when successful, non-zero (FALSE) otherwise when
 *        - s1 as pointer to Student struct is NULL
 *        - s2 as pointer to Student struct is NULL
 */
int swap_students(Student * const s1, Student * const s2){
	if (s1 == NULL || s2 == NULL) return FALSE;

	char tmp_name[strlen(s1->name)];

	strcpy(tmp_name, s1->name);
	strcpy(s1->name, s2->name);
	strcpy(s2->name, tmp_name);

	if (s2->grades != NULL && s1->grades != NULL) {
		char tmp_grades [strlen(s2->grades)];

		strcpy(tmp_grades, s1->grades);
		strcpy(s1->grades, s2->grades);
		strcpy(s2->grades, tmp_grades);
	}

	if (s2->grades == NULL && s1->grades != NULL) {
		s2->grades = (char *)malloc(strlen(s1->grades) * sizeof(char));

		strcpy(s2->grades, s1->grades);
		free(s1->grades);
	}

	if (s1->grades == NULL && s2->grades != NULL) {
		s1->grades = (char *)malloc(strlen(s2->grades) * sizeof(char));

		strcpy(s1->grades, s2->grades);
		free(s2->grades);
	}

	float tmp_mark = s1->mark;
	s1->mark = s2->mark;
	s2->mark = tmp_mark;

	/* to do */
    return TRUE;
}	

 /* 
 * delete Student (free allocated name, grades and student struct)
 * params:
 *	Student * > pointer to Student struct
 * return:
 *	int > 0 (TRUE) when successfull, non-zero (FALSE) otherwise when
 *        - pointer to Student struct is NULL
 */
int del_student(Student *s){

	if (s == NULL) return FALSE;

	if (s->grades != NULL) free(s->grades);

	free(s->name);
	free(s);

	/* to do */
    return TRUE;
}

 /* 
 * print Student
 * params:
 *	Student * > pointer to Student struct
 * return:
 *	int > number of printed characers
 */
int print_student(Student *s){
	if (s == NULL) return FALSE;

	(void)printf("Name: %s, ", s->name);
	(void)printf("Mark: %f", s->mark);

	if (s->grades != NULL) {
		(void)printf(", Grades: %s", s->grades);
	}
	(void)printf("\n");

	return sizeof(s);
}

 /* 
 * list Student
 * params:
 *	Student * > pointer to Student struct
 *  const char * const > delimiter
 * return:
 *	char * > static char buffer[SIZE] as Student listing like:
 *           name[delimiter]mark[delimiter]grades
 */
char *list_student(Student *s, const char * const delimiter){
	int total_length = 0, delimiter_length = strlen(delimiter);

	total_length += strlen(s->name);
	total_length += delimiter_length;
	// marks need two spaces as string
	total_length += 2;

	if (s->grades != NULL) {
		total_length += delimiter_length;
		total_length += strlen(s->grades);
		total_length += delimiter_length;
	}

	char *result = (char *)malloc((total_length + 1) * sizeof(char));


	if (s->grades == NULL) {
		sprintf(result, "%s%s%.1f", s->name, delimiter, s->mark);
	} else {
		sprintf(result, "%s%s%.1f%s%s", s->name, delimiter, s->mark, delimiter, s->grades);
	}

	return result;
}