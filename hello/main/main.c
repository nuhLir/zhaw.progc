/* 
 * Module      : ZHAW PROGC hello
 * Description : hello world sample project
 * $Id: main.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <stdio.h>
#include "../src/hello.h"

/* 
 * hello world main 
 * params:
 *	void
 * return:
 *	int shell exit param
 */
int main(void){	
	(void) printf("\nSay: %s", hello_world());
	(void) printf("\nSay: %s", by_world() );
	(void) printf("\nSay: %s", echo("hello again!"));
	(void) printf("\nlong: %d", sizeof(long long));
	
	return EXIT_SUCCESS;
}