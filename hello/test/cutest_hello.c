/* 
 * Module      : ZHAW PROGC hello
 * Description : CUnit Tests > hello world sample project
 * $Id: cutest_hello.c 2015-07-02 metl$
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h>
#include "hello.h"

int init_hello(void);
int cleanup_hello(void);
void test_hello_world(void);
void test_by_world(void);
void test_echo(void);

int main(void) {
	CU_pSuite suite;
	CU_initialize_registry();
	suite = CU_add_suite("hello", init_hello, cleanup_hello);
	CU_ADD_TEST(suite, test_hello_world);	
	CU_ADD_TEST(suite, test_by_world);	
	CU_ADD_TEST(suite, test_echo);
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return EXIT_SUCCESS;
}

int init_hello(void) {
	return 0;
}

int cleanup_hello(void) {
	return 0;
}

void test_hello_world(void) {
	CU_ASSERT_STRING_EQUAL("hello c world", hello_world());
}

void test_by_world(void) {
	CU_ASSERT_STRING_EQUAL("goodby c world", by_world());
}

void test_echo(void) {
	CU_ASSERT_STRING_EQUAL("hi again", echo("hi again"));
}
