/* 
 * Module      : ZHAW PROGC hello
 * Description : hello world sample project
 * $Id: hello.c 2015-07-02 metl$
 */

#include <stdio.h>

/* 
 * returns hello world
 * params:
 *	void
 * return:
 *	char * 'hello c world'
 */
char *hello_world(void){
	return "hello c world";
}

/* 
 * returns by world
 * params:
 *	void
 * return:
 *	char * 'goodby c world'
 */
char *by_world(void){
	return "goodby c world";
}

/* 
 * echo 
 * params:
 *	char * as input
 * return:
 *	char * returns input
 */
char *echo(char *s){
	return s;
}

