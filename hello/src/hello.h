/* 
 * Module      : ZHAW PROGC hello
 * Description : hello world sample project
 * $Id: hello.h 2015-07-02 metl$
 */

#ifndef HELLO_H_
#define HELLO_H_

char *hello_world(void);
char *by_world(void);
char *echo(char *);

#endif /* HELLO_H_ */
