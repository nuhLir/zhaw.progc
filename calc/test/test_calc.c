/* 
 * Module      : ZHAW PROGC calc
 * Description : CUnit Tests > calc
 * $Id: test_calc.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <stdio.h>
#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h> 
#include <CUnit/TestRun.h> 
#include <CUnit/Automated.h>

#include "calc.h"

void test_add(void);
void test_sub(void);
void test_mul(void);
void test_div(void);
void test_nop(void);
void test_pot(void);

int *test_calc(int *res, char *mode) {
	CU_initialize_registry();

	CU_pSuite suite = CU_add_suite("calc", NULL, NULL);

	CU_ADD_TEST(suite, test_add);	
	CU_ADD_TEST(suite, test_sub);	
	CU_ADD_TEST(suite, test_mul);
	CU_ADD_TEST(suite, test_div);
	CU_ADD_TEST(suite, test_nop);
	CU_ADD_TEST(suite, test_pot);

	if (mode == NULL) {
		CU_basic_set_mode(CU_BRM_VERBOSE);
		CU_basic_run_tests();
	} else {
		CU_set_output_filename(mode);
		CU_automated_run_tests();
	} 
	
	if (res != NULL) {
		*(res + 0) = CU_get_number_of_tests_run();
		*(res + 1) = CU_get_number_of_asserts();
		*(res + 2) = CU_get_number_of_successes();
	}
	
	CU_cleanup_registry();
	
	return res;
}

void test_add(void) {
	CU_ASSERT_DOUBLE_EQUAL(2.0f, add(1.0, 1.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(5.0f, add(2.0, 3.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(-2.1f, add(3.0, -5.1), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(0.0f, add(1.0, -1.0), 0.01f);
}

void test_sub(void) {
	CU_ASSERT_DOUBLE_EQUAL(0.0f, sub(1.0, 1.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(1.0f, sub(2.0, 3.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(-8.1f, sub(3.0, -5.1), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(-2.0f, sub(1.0, -1.0), 0.01f);
}

void test_mul(void) {
	CU_ASSERT_DOUBLE_EQUAL(1.0f, mul(1.0, 1.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(6.0f, mul(2.0, 3.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(-15.3f, mul(3.0, -5.1), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(-1.0f, mul(1.0, -1.0), 0.01f);
}

void test_div(void) {
	CU_ASSERT_DOUBLE_EQUAL(1.0f, dif(1.0, 1.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(1.5f, dif(2.0, 3.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(-0.58f, dif(-5.1, 3), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(-1.0f, dif(1.0, -1.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(0, dif(0, 1), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(0, dif(0, 0), 0.01f);
}

void test_nop(void) {
	CU_ASSERT_DOUBLE_EQUAL(0, nop(1.0, 1.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(0, nop(2.0, 3.0), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(0, nop(3.0, -5.1), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(0, nop(1.0, -1.0), 0.01f);
}

void test_pot(void){
	CU_ASSERT_DOUBLE_EQUAL(4.0f, pot(2.0f, 2.0f), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(16.0f, pot(4.0f, 2.0f), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(6561.0f, pot(8.0f, 3.0f), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(81.0f, pot(4.0f, 3.0f), 0.01f);
	CU_ASSERT_DOUBLE_EQUAL(125.0f, pot(3.0f, 5.0f), 0.01f);
}
