/* 
 * Module      : ZHAW PROGC calc
 * Description : division of two floats
 * $Id: diff.c 2015-07-02 metl$
 */

 #include "calc.h"
  
 /* 
 * division of two floats
 * params:
 *	float > divisor
 *	float > divident
 * return:
 *	float > quotient = divident / divisor, or  
 *           - 0.0f when divisor is 0
 */ 
float dif(float divisor, float divident) {
	if (divisor == 0.0f){
		return -divisor;
	}

	/* to do */
	return divident / divisor;
}
