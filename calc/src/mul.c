/* 
 * Module      : ZHAW PROGC calc
 * Description : multiplication of two floats
 * $Id: mul.c 2015-07-02 metl$
 */
 
 #include "calc.h"

 /* 
 * multiplication of two floats
 * params:
 *	float > multiplicand
 *	float > multiplier
 * return:
 *	float > product = multiplier * multiplicand
 */  
float mul(float multiplicand, float multiplier) {
	/* to do */
	return multiplicand * multiplier;
}
