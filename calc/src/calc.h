/* 
 * Module      : ZHAW PROGC calc
 * Description : calculations
 * $Id: calc.h 2015-07-02 metl$
 */

#ifndef CALC_H_
#define CALC_H_

float add(float, float);
float sub(float, float);
float mul(float, float);
float dif(float, float);
float nop(float, float);
float pot(float, float);

#endif /* CALC_H_ */
