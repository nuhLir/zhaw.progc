/* 
 * Module      : ZHAW PROGC calc
 * Description : multiplication of two floats
 * $Id: mul.c 2015-07-02 metl$
 */
 
 #include "calc.h"

 /* 
 * calculates power of two floats
 * params:
 *	float > power
 *	float > base
 * return:
 *	float > product = base ^ power
 */  
float pot(float power, float base) {
	float result = base;

	for(int i = 0; i < (int)power - 1; i++){
		result *= base;
	}

	// (void)printf("\n%f ^ %d = %f ", base, (int)power, result);
	return result;
}
