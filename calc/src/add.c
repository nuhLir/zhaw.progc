/* 
 * Module      : ZHAW PROGC calc
 * Description : addition of two floats
 * $Id: add.c 2015-07-02 metl$
 */

 #include "calc.h"
 
/* 
 * addition of two floats
 * params:
 *	float > 1st summand
 *	float > 2nd summand
 * return:
 *	float > sum = 2nd summand + 1st summand
 */ 
float add(float summand1, float summand2) {
	/* to do */
	return summand1 + summand2;
}
