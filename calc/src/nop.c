/* 
 * Module      : ZHAW PROGC calc
 * Description : nop = no operation
 * $Id: nop.c 2015-07-02 metl$
 */
 
 #include "calc.h"

  /* 
 * nop with two floats
 * params:
 *	float > 1st float value
 *	float > 2nd float value
 * return:
 *	float > always 0
 */  
float nop(float x, float y) {
	x = y;

	return 0.0f;
}
