/* 
 * Module      : ZHAW PROGC calc
 * Description : subtraction of two floats
 * $Id: sub.c 2015-07-02 metl$
 */
 
 #include "calc.h"

  /* 
 * subtraction of two floats
 * params:
 *	float > subtrahend
 *	float > minuend
 * return:
 *	float > difference = minuend - subtrahend
 */  
float sub(float subtrahend , float minuend) {
	return minuend - subtrahend;
}
