#include <stdlib.h>
#include <stdio.h>
#include "../src/calc.h"

int main( int argc, char* argv[] ){
	float firstValue = 12.0f,
		secondValue = 8.0f,
		result = 0.0f;

	int operation = 1;

	// generate arguments
	if (argc > 2){
		// 1.parameter: The operation as numeric code
		operation = atoi(argv[1]);
		
		// 2.parameter: First value
		firstValue = atof(argv[2]);

		// 3.parameter: Second value
		secondValue = atof(argv[3]);
	}

	// a list of visual representations of the operations
	char operations[] = { '+', '-', '*', '/', 'N', '^' };

	// check which operand to map
	switch(operation) {
		case 1: result = add(firstValue, secondValue); break;
		case 2: result = sub(firstValue, secondValue); break;
		case 3: result = mul(firstValue, secondValue); break;
		case 4: result = dif(firstValue, secondValue); break;
		case 5: result = nop(firstValue, secondValue); break;
		case 6: result = pot(firstValue, secondValue); break;

		default: 
			(void)printf("Unknown operation!\n");
			return EXIT_FAILURE;
			break;
	}

	// render the actual calculation including the result
	(void)printf("%f %c %f = %f\n", 
		firstValue, 
		operations[operation - 1],
		secondValue,
		result);

	return EXIT_SUCCESS;
}