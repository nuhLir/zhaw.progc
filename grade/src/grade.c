/* 
 * Module      : ZHAW PROGC grade
 * Description : convert grades to marks
 * $Id: grade.c 2015-07-02 metl$
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "grade.h"

/* 
 * check Grades if 'A' .. 'D'
 * params:
 *	char * > grades e.g. "ACDB"
 * return:
 *	int > TRUE if success, FALSE otherwise
 */
int check_grades(char grades[]){
	if (grades == NULL) return FALSE;

	int str_len = strlen(grades);

	if (str_len == 0) return FALSE;

	for (int i = 0; i < str_len; i++) {
		char current_grade = grades[i];

		switch(current_grade) {
			case 'A':
			case 'B':
			case 'C':
			case 'D':
				break;
			default:
				return FALSE;
				break;
		}
	}

	/* to do */
	return TRUE;
}

/* 
 * convert Grade to unsigned short Mark
 * params:
 *	Grade > grade e.g. GRADE_A, GRADE_B, ...
 * return:
 *	unsigned short > mark e.g. 6, 5, 4, 3 or 1 (default)
 */
unsigned short grade_to_mark(Grade grade) {
	unsigned short result = 0;

	switch(grade){
		case GRADE_A:
			result = 6;
			break;
		case GRADE_B:
			result = 5;
			break;
		case GRADE_C:
			result = 4;
			break;
		case GRADE_D:
			result = 3;
			break;
		case GRADE_NONE:
		default:
			result = 1;
			break;
	}

	return result;
}

/* 
 * convert Grade to char * Mark
 * params:
 *	Grade > grade e.g. GRADE_A, GRADE_B, ...
 * return:
 *	char * > mark e.g. "6", "5", "4", "3" or "1" (default)
 */
char *get_mark(Grade grade){
	char *result = (char *)malloc(2 * sizeof(char));

	switch(grade){
		case GRADE_A:
		case GRADE_B:
		case GRADE_C:
		case GRADE_D:
			strcpy(result, GRADE_2_STR(grade));
			break;
		default:
			strcpy(result, "1");
			break;
	}

	return result;
}

/* 
 * convert Grade to unsigned short Mark with GRADE_2_MARK (Macro)
 * params:
 *	char > grade e.g. 'A', 'B', 'C', 'D'
 * return:
 *	unsigned short > mark e.g. 6, 5, 4, 3 or 1 (default)
 */
unsigned short grade_2_mark(char grade){
	switch(grade){
		case GRADE_A:
		case GRADE_B:
		case GRADE_C:
		case GRADE_D:
			return GRADE_2_MARK(grade);
			break;
		default:
			return 1;
			break;
	}
}

/* 
 * calculate average mark and round to HALF_NOTE
 * params:
 *	const char * > grades e.g. "AABC"
 * return:
 *	float > average e.g. 5.50, or
 *           - 1.0 without grades
 */
float avg_mark(const char *grades) {
	if (grades == NULL || !check_grades((char *)grades)) return 1.0f;

	int str_len = strlen(grades);
	float sum = 0.0f;

	if (str_len == 0) return 1.0f;

	for (int i = 0; i < str_len; i++) {
		char current_grade = grades[i];

		sum += (float)grade_to_mark(current_grade);
	}

	sum /= str_len;

	return sum;
}

/* 
 * round mark to fraction
 * params:
 *	float > mark e.g. 4.24
 *  unsigned int > fraction e.g. 2 > 1/2
 * return:
 *	float > e.g. , 4.0
 */
float round_mark(float mark, unsigned int fraction) {
	if (mark < 1.0f) return 1.0f;
	if (mark > 6.0f) return 6.0f;

	/* to do */
	return roundf(mark * fraction) / (float)fraction;
}

/* 
 * convert to top n grades
 * params:
 *	char * > grades, e.g. "ACBDAA"
 *  unsigned int > number of grades
 * return:
 *	char * > pointer to new allocated grades, or
 *           - NULL when no grades
 *           - 'grades' when empty grades
 *           - 'grades' when n < 1
 */
char *top_n_grades(char *grades, unsigned int n){
	if (grades == NULL) return NULL;
	if (n < 1) return grades;

	unsigned int grades_length = strlen(grades);

	if (grades_length == 0) return grades;

	// sort the shit first of all
	sort_asc(grades);

	// create string on heap, to have access on it outside the scope
	char *result = (char *)malloc((n + 1) * sizeof(char));

	// copy the stuff from grades into the result array
	strncpy(result, grades, n);

	// check special case
	if (n > grades_length) {
		for (unsigned int i = grades_length; i < n; i++) {
			result[i] = GRADE_D;
		}
		result[n] = 0;
	}

	return result;
}
	
/* 
 * sort grades ascending
 * params:
 *	const char * > grades e.g. "ACDBABC" > "AABBCCD" 
 * return:
 *	void 
 */
void sort_asc(char *const grades) {
	int grades_length = strlen(grades);

	if (grades_length == 0) return;

	for (int i = 0; i < grades_length - 1; i++) {
		for (int j = i + 1; j < grades_length; j++) {
			if (grades[i] > grades[j]){
				char tmp = grades[i];

				grades[i] = grades[j];
				grades[j] = tmp;
			}
		}
	}
} 

/* 
 * calculate the average grades with variable argument list
 * params:
 *	unsigned n > number of arguments (e.g. 3) 
 *  ...        > variable argument list (grades e.g. 'A', 'B', 'C')
 * return:
 *	float > the average or 0
 *           - when zero arguments 
 *           - grades not valid (not 'A' .. 'D')
 */
float avg (unsigned n, ...) { 
	va_list args;
	char grade = 0;
	float sum = 0.0f;
	unsigned int i = 0;

	va_start(args, n);

	do {
   		i++;
		grade = (char)va_arg(args, int);
		unsigned short current_grade_value = grade_2_mark(grade);

		// invalid arguments cause the whole process to quit with 0.0f result
		if (current_grade_value == 1) return 0.0f;

		sum += (float)current_grade_value;
	} while((int)grade != 0 && i < n);

   va_end(args);

   return sum / (float)n;
}



