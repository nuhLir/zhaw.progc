/* 
 * Module      : ZHAW PROGC grade
 * Description : convert grades to marks
 * $Id: grade.h 2015-07-02 metl$
 */
 
#ifndef GRADE_H_
#define GRADE_H_

#define GRADE_2_MARK(grade)	(char)(~grade - 0xb8)
#define GRADE_2_STR(grade)	(char[2]) { (char)(~grade - 0x88), '\0' }

#define HALF_NOTE	2

#ifndef FALSE
#define FALSE 		0
#define TRUE 		!FALSE
#endif /* FALSE */

typedef enum {
    GRADE_NONE      = '\0',
    GRADE_A         = 'A',
    GRADE_B         = 'B',
    GRADE_C         = 'C',
    GRADE_D         = 'D',
    GRADE_DEFAULT   = GRADE_D
} Grade;

int check_grades(char[]);
unsigned short grade_to_mark(Grade);
char *get_mark(Grade);
unsigned short grade_2_mark(char);
float avg_mark(const char *);
float round_mark(float, unsigned int);
char *top_n_grades (char *, unsigned int);
void sort_asc(char *const);
float avg (unsigned, ...);

#endif /* GRADE_H_ */
