/* 
 * Module      : ZHAW PROGC grade
 * Description : convert grades to marks
 * $Id: main.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <stdio.h>
#include "../src/grade.h"

int main(void){	
	char grade;
	grade = 'A';
	(void) printf("\nMark %d is equal to Grade %c", grade_2_mark(grade), grade );
	(void) printf("\nMark %d is equal to Grade %c", grade_to_mark(grade), grade );
	(void) printf("\nMark %s is equal to Grade %c", get_mark(grade), grade );

	grade = 'B';
	(void) printf("\nMark %d is equal to Grade %c", grade_2_mark(grade), grade );

	grade = 'C';
	(void) printf("\nMark %d is equal to Grade %c", grade_2_mark(grade), grade );

	grade = 'D';
	(void) printf("\nMark %d is equal to Grade %c", grade_2_mark(grade), grade );

	grade = 'E';
	(void) printf("\nMark %d is equal to Grade %c", grade_2_mark(grade), grade );

	grade = 'a';
	(void) printf("\nMark %d is equal to Grade %c", grade_2_mark(grade), grade );
	
	(void) printf("\nThe Grades %s average to the Mark %.2f", "ABCD", avg_mark("ABCD"));	
	
	char grades[] = "ABCDABCD";
	sort_asc(grades);
	(void) printf("\nThe Grades ABCDABCD sort ascending to %s", grades);
}