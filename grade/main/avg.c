/* 
 * Module      : ZHAW PROGC grade
 * Description : convert grades to marks
 * $Id: main.c 2015-07-02 metl$
 */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#define GRADE_2_MARK(grade)	(char)(~grade - 0xb8)

float avg (unsigned n, ...) { 
  va_list grades;
  unsigned i;
  int grade;
  float sum = 0.0f;
  va_start(grades, n);	

  for (i = 1; i <= n; i++) {
    grade = va_arg(grades, int); 
	sum += GRADE_2_MARK(grade);
  }
  va_end(grades); 	
  return (n ? sum / n : 0.0f);
}
int main(void) {
  (void) printf ("avg = %.1f\n", avg(0));
  (void) printf ("avg = %.1f\n", avg(1, 0x41));	
  (void) printf ("avg = %.1f\n", avg(3, 0x41, 0x42, 0x43));
  (void) printf ("avg = %.1f\n", avg(4, 'A', 'B', 'C', 'D'));

}
