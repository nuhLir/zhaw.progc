/* 
 * Module      : ZHAW PROGC grade
 * Description : CUnit Tests > grade
 * $Id: test_grade.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h>
#include <CUnit/Automated.h>
#include "grade.h"

int init_grade(void);
int cleanup_grade(void);
void test_check_grades(void);
void test_grade_to_mark(void);
void test_grade_2_mark(void);
void test_avg_mark(void);
void test_round_mark(void);
void test_top_n_grades(void);
void test_sort_asc(void);
void test_avg(void);
void test_get_mark(void);

static char grades[10] = "";

int *test_grade(int *res, char *mode) {
	CU_initialize_registry();

	CU_pSuite suite = CU_add_suite("grade", init_grade, cleanup_grade);

	CU_ADD_TEST(suite, test_check_grades);	
	CU_ADD_TEST(suite, test_grade_to_mark);	
	CU_ADD_TEST(suite, test_grade_2_mark);
	CU_ADD_TEST(suite, test_avg_mark);
	CU_ADD_TEST(suite, test_round_mark);
	CU_ADD_TEST(suite, test_top_n_grades);
	CU_ADD_TEST(suite, test_sort_asc);
	CU_ADD_TEST(suite, test_avg);
	CU_ADD_TEST(suite, test_get_mark);
	
	if (mode == NULL) {
		CU_basic_set_mode(CU_BRM_VERBOSE);
		CU_basic_run_tests();
	} else {
		CU_set_output_filename(mode);
		CU_automated_run_tests();
	} 
	
	if (res != NULL) {
		*(res + 0) = CU_get_number_of_tests_run();
		*(res + 1) = CU_get_number_of_asserts();
		*(res + 2) = CU_get_number_of_successes();
	}
	
	CU_cleanup_registry();
	return res;
}

int init_grade(void) {
	strcpy(grades, "");
	return 0;
}

int cleanup_grade(void) {
	strcpy(grades, "");
	return 0;
}

void test_check_grades(void){
	CU_ASSERT_FALSE(check_grades(NULL));
	CU_ASSERT_FALSE(check_grades(""));
	CU_ASSERT_FALSE(check_grades("a"));
	CU_ASSERT_FALSE(check_grades("1"));
	CU_ASSERT_FALSE(check_grades("-3"));
	CU_ASSERT_FALSE(check_grades("ABCr"));
	CU_ASSERT_FALSE(check_grades("AbC"));
	CU_ASSERT_FALSE(check_grades("ABCDEF1"));
	CU_ASSERT_FALSE(check_grades("ABC "));
	
	CU_ASSERT_TRUE(check_grades("A"));
	CU_ASSERT_TRUE(check_grades("B"));
	CU_ASSERT_TRUE(check_grades("C"));
	CU_ASSERT_TRUE(check_grades("D"));
	CU_ASSERT_TRUE(check_grades("AB"));
	CU_ASSERT_TRUE(check_grades("AAAAABBB"));
	CU_ASSERT_TRUE(check_grades("ABCD"));
	CU_ASSERT_TRUE(check_grades("DADCBADCBDAC"));
}

void test_grade_to_mark(void) {
	CU_ASSERT(6 == grade_to_mark('A'));
	CU_ASSERT(5 == grade_to_mark('B'));
	CU_ASSERT(4 == grade_to_mark('C'));
	CU_ASSERT(3 == grade_to_mark('D'));
	CU_ASSERT(1 == grade_to_mark('F'));
	CU_ASSERT(1 == grade_to_mark(0x40));
	CU_ASSERT(1 == grade_to_mark('a'));
	CU_ASSERT(1 == grade_to_mark(0));
}

void test_grade_2_mark(void) {
	CU_ASSERT(6 == grade_2_mark('A'));
	CU_ASSERT(5 == grade_2_mark('B'));
	CU_ASSERT(4 == grade_2_mark('C'));
	CU_ASSERT(3 == grade_2_mark('D'));
	CU_ASSERT(1 == grade_2_mark('F'));
	CU_ASSERT(1 == grade_2_mark(0x40));
	CU_ASSERT(1 == grade_2_mark('a'));
	CU_ASSERT(1 == grade_2_mark(0));
	CU_ASSERT(1 == grade_2_mark(1));
	CU_ASSERT(1 == grade_2_mark(2));
	CU_ASSERT(1 == grade_2_mark(-1));
	CU_ASSERT(1 == grade_2_mark(100));
	CU_ASSERT(1 == grade_2_mark(0xFF));
	CU_ASSERT(1 == grade_2_mark((char)1.5));
}

void test_avg_mark(void) {
	CU_ASSERT_DOUBLE_EQUAL(1.0, avg_mark(""), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(1.0, avg_mark(NULL), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(6.0, avg_mark("A"), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(4.5, avg_mark("ABCD"), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(1.0, avg_mark("a"), 0.1f);
}

void test_round_mark(void) {
	CU_ASSERT_DOUBLE_EQUAL(5.0, round_mark(4.75f, 2), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(4.5, round_mark(4.74f, 2), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(4.5, round_mark(4.25f, 2), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(4.0, round_mark(4.24f, 2), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(1.0, round_mark(0, 2), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(1.0, round_mark(-1, 2), 0.1f);
}

void test_top_n_grades(void) {
	CU_ASSERT(NULL == top_n_grades(NULL, 5));
	CU_ASSERT_STRING_EQUAL("", strcpy(grades, ""));
	CU_ASSERT(grades == top_n_grades(grades, 0));
	CU_ASSERT_STRING_EQUAL("A", strcpy(grades, "A"));
	CU_ASSERT(grades == top_n_grades(grades, 0));
	CU_ASSERT_STRING_EQUAL("A", strcpy(grades, "A"));
	CU_ASSERT_STRING_EQUAL("ADDDD", top_n_grades(grades, 5));
	CU_ASSERT_STRING_EQUAL("A", strcpy(grades, "A"));
	CU_ASSERT_STRING_EQUAL("ADDDDD", top_n_grades(grades, 6));
	CU_ASSERT_STRING_EQUAL("DCBA", strcpy(grades, "DCBA"));
	CU_ASSERT_STRING_EQUAL("ABCDD", top_n_grades(grades, 5));
	CU_ASSERT_STRING_EQUAL("ADBCCDCBA", strcpy(grades, "ADBCCDCBA"));
	CU_ASSERT_STRING_EQUAL("AABBC", top_n_grades(grades, 5));
	CU_ASSERT_STRING_EQUAL("abc", strcpy(grades, "abc"));
	CU_ASSERT_STRING_EQUAL("abcDD", top_n_grades(grades, 5));
	CU_ASSERT_STRING_EQUAL("z", strcpy(grades, "z"));
	CU_ASSERT_STRING_EQUAL("zDDDD", top_n_grades(grades, 5));
}

void test_sort_asc(void) {
	CU_ASSERT_STRING_EQUAL("DCBA", strcpy(grades, "DCBA"));
	sort_asc(grades);
	CU_ASSERT_STRING_EQUAL("ABCD", grades);

	CU_ASSERT_STRING_EQUAL("ABCDABCD", strcpy(grades, "ABCDABCD"));
	sort_asc(grades);
	CU_ASSERT_STRING_EQUAL("AABBCCDD", grades);
	
	CU_ASSERT_STRING_EQUAL("", strcpy(grades, ""));
	sort_asc(grades);
	CU_ASSERT_STRING_EQUAL("", grades);
}

void test_avg(void) {
	CU_ASSERT_DOUBLE_EQUAL(4.5, avg(4, 'A', 'B', 'C', 'D'), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(5.3, avg(3, 'A', 'A', 'C'), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(3.0, avg(2, 'D', 'D'), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(0, avg(4, 'a', 'b', 'c', 'd'), 0.1f);
}

void test_get_mark(void) {
	CU_ASSERT_STRING_EQUAL("6", get_mark('A'));
	CU_ASSERT_STRING_EQUAL("5", get_mark('B'));
	CU_ASSERT_STRING_EQUAL("4", get_mark('C'));
	CU_ASSERT_STRING_EQUAL("3", get_mark('D'));

	CU_ASSERT_STRING_EQUAL("1", get_mark('a'));
	CU_ASSERT_STRING_EQUAL("1", get_mark('z'));
	CU_ASSERT_STRING_EQUAL("1", get_mark('b'));
	CU_ASSERT_STRING_EQUAL("1", get_mark('c'));
	CU_ASSERT_STRING_EQUAL("1", get_mark('?'));
	CU_ASSERT_STRING_EQUAL("1", get_mark('/'));

	CU_ASSERT_FALSE(strcmp("6", get_mark('A')));
	CU_ASSERT_FALSE(strcmp("5", get_mark('B')));
	CU_ASSERT_FALSE(strcmp("4", get_mark('C')));
	CU_ASSERT_FALSE(strcmp("3", get_mark('D')));
	CU_ASSERT_FALSE(strcmp("1", get_mark('&')));
	CU_ASSERT_FALSE(strcmp("1", get_mark('a')));
	CU_ASSERT_FALSE(strcmp("1", get_mark('b')));
	CU_ASSERT_FALSE(strcmp("1", get_mark('E')));

	CU_ASSERT_STRING_EQUAL("6", get_mark(GRADE_A));
	CU_ASSERT_STRING_EQUAL("5", get_mark(GRADE_B));
	CU_ASSERT_STRING_EQUAL("4", get_mark(GRADE_C));
	CU_ASSERT_STRING_EQUAL("3", get_mark(GRADE_D));
	CU_ASSERT_STRING_EQUAL("3", get_mark(GRADE_DEFAULT));
}
