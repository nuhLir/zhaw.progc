/* 
 * Module      : ZHAW PROGC file
 * Description : CUnit Tests > file
 * $Id: test_file.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <string.h>
#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h> 
#include <CUnit/Automated.h>
#include "file.h"

int init_file(void);
int cleanup_file(void);
void test_write_line(void);
void test01_read_line(void);
void test02_read_line(void);

static FILE *fp;
static char *FILENAME = "test_read_line.txt";

#define LENGTH	256
static char line[LENGTH];

int *test_file(int *res, char *mode) {
	CU_pSuite suite;
	CU_initialize_registry();
	suite = CU_add_suite("read_line", init_file, cleanup_file);
	CU_ADD_TEST(suite, test_write_line);	
	CU_ADD_TEST(suite, test01_read_line);	
	CU_ADD_TEST(suite, test02_read_line);
	
	if (mode == NULL) {
		CU_basic_set_mode(CU_BRM_VERBOSE);
		CU_basic_run_tests();
	} else {
		CU_set_output_filename(mode);
		CU_automated_run_tests();
	} 
	
	if (res != NULL) {
		*(res + 0) = CU_get_number_of_tests_run();
		*(res + 1) = CU_get_number_of_asserts();
		*(res + 2) = CU_get_number_of_successes();
	}
	
	CU_cleanup_registry();
	return res;
}

int init_file(void) {
	OPEN_FILE(fp, FILENAME, "w+")
	return 0;
}

int cleanup_file(void) {
	CLOSE_FILE(fp);
	remove(FILENAME);
	return 0;
}

void test_write_line(void) {
	CU_ASSERT_PTR_EQUAL(line, strcpy(line, "1st line"));
	CU_ASSERT_FALSE(EOF == write_line(fp, line));
	CU_ASSERT_PTR_EQUAL(line, strcpy(line, "2nd line"));
	CU_ASSERT_FALSE(EOF == write_line(fp, line));
	CU_ASSERT_PTR_EQUAL(line, strcpy(line, "3rd line"));
	CU_ASSERT_FALSE(EOF == write_line(fp, line));
}

void test01_read_line(void) {
	char *lp = line;
	rewind(fp); 
	CU_ASSERT_STRING_EQUAL(read_line(fp, lp), "1st line");
	CU_ASSERT_STRING_EQUAL(read_line(fp, lp), "2nd line");
	CU_ASSERT_STRING_EQUAL(read_line(fp, lp), "3rd line");
	CU_ASSERT(0 == read_line(fp, lp));
}

void test02_read_line(void) {
	char *lp = line;
	rewind(fp);  
	CU_ASSERT_STRING_EQUAL(read_line(fp, lp), "1st line");
	CU_ASSERT_STRING_EQUAL(read_line(fp, lp), "2nd line");
	CU_ASSERT_STRING_EQUAL(read_line(fp, lp), "3rd line");
	CU_ASSERT(0 == read_line(fp, lp));
	CU_ASSERT(0 == read_line(fp, lp));
}
