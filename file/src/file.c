/* 
 * Module      : ZHAW PROGC file
 * Description : file handling
 * $Id: file.c 2015-07-02 metl$
 */
 
#include <stdio.h>
#include <string.h>
#include "file.h"

/* 
 * read a line form file
 * terminate with '\0', drop '\n'
 * params:
 *	FILE * > pointer to file
 *  char * > pointer to buffer 
 * return:
 *	char * > pointer to buffer (line by line), or
 *           - NULL by End Of File  
 */ 
char *read_line(FILE *fp, char *buffer) {
	/* to do */
	return "";
}

/* 
 * write a line to file
 * terminate with '\n'
 * params:
 *	FILE * > pointer to file
 *  char * > pointer to buffer 
 * return:
 *	int    > on success non-negative value, or
 *           - EOF otherwise
 */ 
int write_line(FILE *fp, char *buffer) {
	/* to do */
	return 0;
}

