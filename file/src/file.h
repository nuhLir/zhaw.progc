/* 
 * Module      : ZHAW PROGC file
 * Description : file handling
 * $Id: file.h 2015-07-02 metl$
 */

#ifndef FILE_H_
#define FILE_H_
#include <stdio.h>

#define BUFFER_SIZE	256

// open file
#define OPEN_FILE(handler, file, mode)	if((handler = fopen(file, mode)) == NULL){ \
											(void) fprintf(stderr, "can not open file %s\n", file); \
											return -1; \
										}
								      
// close file
#define CLOSE_FILE(handler)	fclose(handler);

char *read_line(FILE *fp, char *buffer);
int write_line(FILE *fp, char *buffer);

#endif /* FILE_H_ */
