/* 
 * Module      : ZHAW PROGC file
 * Description : read and write to file
 * $Id: main.c 2015-07-02 metl$
 */

#include <stdio.h>
#include <stdlib.h>
#include "../src/file.h"

#define SRC 	"../../classes/IT14a_ZH.grades"
#define DST 	"../../classes/IT14a_ZH.marks"

#define LENGTH	256
static char line[LENGTH];

int main(void) {
	
	char *lp = line;
	FILE *f_src, *f_dst;
	OPEN_FILE(f_src, SRC, "r")
	OPEN_FILE(f_dst, DST, "w")
	
	while (NULL != (lp = read_line(f_src, lp))) {
		// puts adds '\n' automatically 
		puts(lp);
		// write_line uses fputs and adds '\n' manually 
		(void) write_line(f_dst, lp);
	}
	
	CLOSE_FILE(f_src)
	CLOSE_FILE(f_dst)

	return EXIT_SUCCESS;
}   
   
   

  