/* 
 * Module      : ZHAW PROGC stack
 * Description : CUnit Tests > stack
 * $Id: test_stack.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <stdio.h>
#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h>
#include <CUnit/Automated.h>
#include "stack.h"

/* redirect stdout to file */
#define STDOUT_FILE		freopen("test_stdout.txt", "w", stdout);
/* redirect stdout to console */
#define STDOUT_CONSOLE	freopen("CONOUT$", "w", stdout);

void test_push(void);
void test_pop(void);
void test_get(void);
void test_set(void);
void test_list_res(void);
void test_clear(void);
void test_invalid_pos(void);


int *test_stack(int *res, char *mode) {
	CU_initialize_registry();

	CU_pSuite suite = CU_add_suite("stack", NULL, NULL);

	CU_ADD_TEST(suite, test_push);	
	CU_ADD_TEST(suite, test_pop);	
	CU_ADD_TEST(suite, test_get);
	CU_ADD_TEST(suite, test_set);
	CU_ADD_TEST(suite, test_list_res);
	CU_ADD_TEST(suite, test_clear);
	CU_ADD_TEST(suite, test_invalid_pos);
	
	if (mode == NULL) {
		CU_basic_set_mode(CU_BRM_VERBOSE);
		CU_basic_run_tests();
	} else {
		CU_set_output_filename(mode);
		CU_automated_run_tests();
	} 
	
	if (res != NULL) {
		*(res + 0) = CU_get_number_of_tests_run();
		*(res + 1) = CU_get_number_of_asserts();
		*(res + 2) = CU_get_number_of_successes();
	}
	
	CU_cleanup_registry();
	return res;
}

void test_push(void) {
	push(1.0f);
	push(2.0f);
	push(3.0f);
	CU_ASSERT_DOUBLE_EQUAL(1.0f, get(2), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(2.0f, get(1), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(3.0f, get(0), 0.1f);
}

void test_pop(void) {
	CU_ASSERT_DOUBLE_EQUAL(3.0f, pop(), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(1.0f, get(1), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(2.0f, get(0), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(2.0f, pop(), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(1.0f, pop(), 0.1f);
}

void test_get(void) {
	push(1.0f);
	push(2.0f);
	push(3.0f);
	CU_ASSERT_DOUBLE_EQUAL(1.0f, get(2), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(2.0f, get(1), 0.1f);
	CU_ASSERT_DOUBLE_EQUAL(3.0f, get(0), 0.1f);	
}

void test_set(void) {
	set(2.1f, 2);
	CU_ASSERT_DOUBLE_EQUAL(2.1f, get(2), 0.1f);
	set(0, -1);
	CU_ASSERT_DOUBLE_EQUAL(2.1f, get(2), 0.1f);
	set(0, STACK_LENGTH);
	CU_ASSERT_DOUBLE_EQUAL(2.1f, get(2), 0.1f);
}

void test_invalid_pos(void) {
	set(42.0f, STACK_LENGTH * 2);
	CU_ASSERT_DOUBLE_EQUAL(0.0f, get(STACK_LENGTH * 2), 0.0f);
	CU_ASSERT_DOUBLE_EQUAL(0.0f, get(-1), 0.0f);
	CU_ASSERT_DOUBLE_EQUAL(0.0f, get(STACK_LENGTH), 0.0f);

	CU_ASSERT_FALSE(validate_pos(STACK_LENGTH));
	CU_ASSERT_TRUE(validate_pos(0));
}

void test_list_res(void) {
	STDOUT_FILE
	int res = list();
	printf("\nAnzahl Zeichen: %d", res);
	STDOUT_CONSOLE
	CU_ASSERT(135 == res);
}

void test_clear(void) {
	clear();
	CU_ASSERT_DOUBLE_EQUAL(0.0f, get(0), 0.1f);	
}

