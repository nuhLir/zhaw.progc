#include <stdlib.h>
#include <stdio.h>
#include "../src/stack.h"

int main( void ){
	int count_pass = 0;

	(void)printf("Wieviele Eintrage soll der Stack beinhalten?");
	scanf("%d", &count_pass);

	if (validate_pos(count_pass) == 0){
		(void)printf("\nDie Zahl ist nicht gueltig. Nehme Default-Wert 10\n");
	}

	for (int i = 0; i <= count_pass; i++){
		push((float)i);
	}

	printf("\n");
	list();
	printf("\n");
	clear();

	return EXIT_SUCCESS;
}