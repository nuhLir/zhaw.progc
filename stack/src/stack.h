/* 
 * Module      : ZHAW PROGC stack
 * Description : stack for float values 
 * $Id: stack.h 2015-07-02 metl$
 */

#ifndef STACK_H_
#define STACK_H_

/* stack size */
#define STACK_LENGTH 15

int validate_pos(int);
void push(float);
float pop();
float get(int);
void set(float, int);
int list();
void clear();

#endif /* STACK_H_ */
