/* 
 * Module      : ZHAW PROGC stack
 * Description : stack for float values 
 * $Id: stack.c 2015-07-02 metl$
 */
#include <stdio.h>
#include "stack.h"

/* initialize stack for float values */
static float stack[STACK_LENGTH] = { 0.0 };

/* 
 * validates the specified position and checks whether it is in the stack
 * params:
 *	int > position
 * return:
 *	int
 */
int validate_pos(int pos){
	return pos < STACK_LENGTH && pos >= 0;
}

/* 
 * increase stack and push new float into stack[0]
 * params:
 *	float > new value to push to position 0
 * return:
 *	void
 */
void push(float new) {
	// first of all move up all existing entries
	// we start from the bottom to start
	for (int i = STACK_LENGTH - 1; i > 0; i--) {
		stack[i] = stack[i - 1];
	}

	// after moving up, set the value into the first position
	stack[0] = new;
}

/* 
 * pop float at position 0 and decrease stack 
 * params:
 *	void
 * return:
 *	float > value at position 0
 */
float pop(void) {
	// store the result first
	float result = stack[0];

	// then continue to move down the stuff
	for (int i = 0; i < STACK_LENGTH - 1; i++){
		stack[i] = stack[i+1];
	}

	// clear the last one
	stack[STACK_LENGTH - 1] = 0.0f;

	return result;
}

/* 
 * get stack at pos 
 * params:
 *	int > stack position 
 * return:
 *	float > value at position pos
 */
float get(int pos) {
	if (!validate_pos(pos)) return 0.0f;

	/* to do */
	return stack[pos];
}

/* 
 * set float value in stack at pos
 * params:
 *  float > value to set at position pos
 *	int > stack position 
 * return:
 *	void
 */
void set(float value, int pos) {
	// validate
	if (!validate_pos(pos)) return;

	stack[pos] = value;
}

/* 
 * list stack to console
 * params:
 *  void 
 * return:
 *	int > number of characters printed
 */
int list(void) {
	int chars = 0;

	for (int i = 0; i < STACK_LENGTH; i++){
		chars += printf("%f\n", stack[i]);
	}

	return chars;
}

/* 
 * clear stack
 * params:
 *  void 
 * return:
 *	void 
 */
void clear(void) {
	for (int i = 0; i < STACK_LENGTH; i++){
		stack[i] = 0.0f;
	}
}
