/* 
 * Module      : ZHAW PROGC list
 * Description : CUnit Tests > list
 * $Id: test_list.c 2015-07-02 metl$
 */

#include <stdlib.h>
#include <stdio.h>
#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h>
#include <CUnit/Automated.h>
#include "list.h"


int i1 = 1, i2 = 2, i3 = 3;
int *e1 = &i1, *e2 = &i2, *e3 = &i3;
Node *n1, *n2, *n3;

int init_list(void);
int cleanup_list(void);

void test_new_node(void);
void test_push_head(void);
void test_push_tail(void);
void test_pop_head(void);
void test_pop_tail(void);
void test_insert_after(void);
void test_insert_before(void);
void test_get_pos(void);

int *test_list(int *res, char *mode) {
	CU_pSuite suite;
	CU_initialize_registry();

	suite = CU_add_suite("list", init_list, cleanup_list);

	CU_ADD_TEST(suite, test_new_node);	
	CU_ADD_TEST(suite, test_push_head);
	CU_ADD_TEST(suite, test_push_tail);
	CU_ADD_TEST(suite, test_pop_head);
	CU_ADD_TEST(suite, test_pop_tail);
	CU_ADD_TEST(suite, test_insert_after);
	CU_ADD_TEST(suite, test_insert_before);
	CU_ADD_TEST(suite, test_get_pos);
	
	if (mode == NULL) {
		CU_basic_set_mode(CU_BRM_VERBOSE);
		CU_basic_run_tests();
	} else {
		CU_set_output_filename(mode);
		CU_automated_run_tests();
	} 
	
	if (res != NULL) {
		*(res + 0) = CU_get_number_of_tests_run();
		*(res + 1) = CU_get_number_of_asserts();
		*(res + 2) = CU_get_number_of_successes();
	}
	
	CU_cleanup_registry();
	return res;
}

int init_list(void){
	n1 = new_node((void *)e1);
	n2 = new_node((void *)e2);
	n3 = new_node((void *)e3);
	return 0;
}

int cleanup_list(void){
    erase_node(n1);
    erase_node(n2);
	erase_node(n3);
	return 0;
}

void test_new_node(void) {
	Node *n = new_node((void *)e1);
    CU_ASSERT_PTR_NOT_NULL(n);

	if  (NULL != n) {
		CU_ASSERT_PTR_NOT_NULL(n->e);
		CU_ASSERT_PTR_NULL(n->prev);
		CU_ASSERT_PTR_NULL(n->next);
		CU_ASSERT(i1 == *(int*)(n->e));
	}
	CU_ASSERT_PTR_NULL(new_node(NULL));
}

void test_push_head(void){
	CU_ASSERT_PTR_NOT_NULL(n1 = new_node((void *)e1));
	CU_ASSERT_PTR_NOT_NULL(n2 = new_node((void *)e2));
	CU_ASSERT_PTR_NOT_NULL(n3 = new_node((void *)e3));
	CU_ASSERT(0 == list_size());
	CU_ASSERT_PTR_NULL(push_head(NULL));
	CU_ASSERT(n1 == push_head(n1));
	CU_ASSERT_PTR_NOT_NULL(n1);
	// check for consistency 
	CU_ASSERT_PTR_NULL(push_head(n1));
	CU_ASSERT(n2 == push_head(n2));
	CU_ASSERT_PTR_NOT_NULL(n2);
	CU_ASSERT(n3 == push_head(n3));
	CU_ASSERT_PTR_NOT_NULL(n3);
	CU_ASSERT(3 == list_size());

	if (list_size() > 0) {
		CU_ASSERT(i3 == *(int *)(pop_head()));
		CU_ASSERT(i2 == *(int *)(pop_head()));
		CU_ASSERT(i1 == *(int *)(pop_head()));
	}
	CU_ASSERT(0 == list_size());
}

void test_push_tail(void){
	CU_ASSERT_PTR_NOT_NULL(n1 = new_node((void *)e1));
	CU_ASSERT_PTR_NOT_NULL(n2 = new_node((void *)e2));
	CU_ASSERT_PTR_NOT_NULL(n3 = new_node((void *)e3));
	CU_ASSERT(0 == list_size());
	CU_ASSERT_PTR_NULL(push_tail(NULL));
	CU_ASSERT(n1 == push_tail(n1));
	CU_ASSERT_PTR_NOT_NULL(n1);
	// check for consistency 
	CU_ASSERT_PTR_NULL(push_tail(n1));
	CU_ASSERT(n2 == push_tail(n2));
	CU_ASSERT_PTR_NOT_NULL(n2);
	CU_ASSERT(n3 == push_tail(n3));
	CU_ASSERT_PTR_NOT_NULL(n3);
	CU_ASSERT(3 == list_size());
	if (list_size() > 0) {
		CU_ASSERT(i3 == *(int *)(pop_tail()));
		CU_ASSERT(i2 == *(int *)(pop_tail()));
		CU_ASSERT(i1 == *(int *)(pop_tail()));
	}
	CU_ASSERT(0 == list_size());
}

void test_pop_head(void){
	CU_ASSERT_PTR_NOT_NULL(n1 = new_node((void *)e1));
	CU_ASSERT(0 == list_size());
	CU_ASSERT_PTR_NULL(pop_head());
	CU_ASSERT(n1 == push_head(n1));
	CU_ASSERT(1 == list_size());
	if (list_size() > 0) {
		CU_ASSERT(i1 == *(int *)(pop_head()));
	}
	CU_ASSERT(0 == list_size());
}

void test_pop_tail(void){
	CU_ASSERT_PTR_NOT_NULL(n1 = new_node((void *)e1));
	CU_ASSERT(0 == list_size());
	CU_ASSERT_PTR_NULL(pop_tail());
	CU_ASSERT(n1 == push_tail(n1));
	CU_ASSERT(1 == list_size());
	if (list_size() > 0) {
		CU_ASSERT(i1 == *(int *)(pop_tail()));
	}
	CU_ASSERT(0 == list_size());
}

void test_insert_after(void){
	CU_ASSERT_PTR_NOT_NULL(n1 = new_node((void *)e1));
	CU_ASSERT_PTR_NOT_NULL(n2 = new_node((void *)e2));
	CU_ASSERT_PTR_NOT_NULL(n3 = new_node((void *)e3));
    CU_ASSERT(0 == list_size());  
	CU_ASSERT(n1 == push_head(n1));
	CU_ASSERT_PTR_NOT_NULL(n1);
	CU_ASSERT(n2 == push_head(n2));
	CU_ASSERT_PTR_NOT_NULL(n2);
	CU_ASSERT(2 == list_size());
	// get Node at position 1
	Node *pos_1_node = get_pos(1);
	// try to insert NULL Pointer
	CU_ASSERT_PTR_NULL(insert_after(pos_1_node, NULL));
	// insert at tail position
	CU_ASSERT(n3 == insert_after(NULL, n3));
	CU_ASSERT_PTR_NOT_NULL(n3);
	CU_ASSERT(3 == list_size());
	if (list_size() > 0) {
		CU_ASSERT(i3 == *(int *)(pop_tail()));
	}
	CU_ASSERT(2 == list_size());
	// insert after position 1
	CU_ASSERT_PTR_NOT_NULL(n3 = new_node((void *)e3));
	CU_ASSERT_PTR_NOT_NULL(n3);
	CU_ASSERT(n3 == insert_after(pos_1_node, n3));
    CU_ASSERT(3 == list_size());
	if (list_size() > 0) {
		CU_ASSERT(i1 == *(int *)(pop_tail()));
		CU_ASSERT(i3 == *(int *)(pop_tail()));
		CU_ASSERT(i2 == *(int *)(pop_tail()));
	}
	CU_ASSERT(0 == list_size());
}

void test_insert_before(void){
	CU_ASSERT_PTR_NOT_NULL(n1 = new_node((void *)e1));
	CU_ASSERT_PTR_NOT_NULL(n2 = new_node((void *)e2));
	CU_ASSERT_PTR_NOT_NULL(n3 = new_node((void *)e3));
    CU_ASSERT(0 == list_size());  
	CU_ASSERT(n1 == push_head(n1));
	CU_ASSERT_PTR_NOT_NULL(n1);
	CU_ASSERT(n2 == push_head(n2));
	CU_ASSERT_PTR_NOT_NULL(n2);
	CU_ASSERT(2 == list_size());
	// get Node at position 1
	Node *pos_1_node = get_pos(1);
	// try to insert NULL Pointer
	CU_ASSERT_PTR_NULL(insert_before(pos_1_node, NULL));
	// insert at tail position
	CU_ASSERT(n3 == insert_before(NULL, n3));
	CU_ASSERT_PTR_NOT_NULL(n3);
	CU_ASSERT(3 == list_size());
	if (list_size() > 0) {
		CU_ASSERT(i3 == *(int *)(pop_head()));
	}
	CU_ASSERT(2 == list_size());
	// insert before position 1
	CU_ASSERT_PTR_NOT_NULL(n3 = new_node((void *)e3));
	CU_ASSERT(n3 == insert_before(pos_1_node, n3));
	CU_ASSERT_PTR_NOT_NULL(n3);
    CU_ASSERT(3 == list_size());
	if (list_size() > 0) {
		CU_ASSERT(i3 == *(int *)(pop_head()));
		CU_ASSERT(i2 == *(int *)(pop_head()));
		CU_ASSERT(i1 == *(int *)(pop_head()));
	}
	CU_ASSERT(0 == list_size());
}

void test_get_pos(void){
	CU_ASSERT_PTR_NOT_NULL(n1 = new_node((void *)e1));
	CU_ASSERT_PTR_NOT_NULL(n2 = new_node((void *)e2));
	CU_ASSERT_PTR_NOT_NULL(n3 = new_node((void *)e3));
    CU_ASSERT(0 == list_size());  
	CU_ASSERT(n1 == push_tail(n1));
	CU_ASSERT_PTR_NOT_NULL(n1);
	CU_ASSERT(n2 == push_tail(n2));
	CU_ASSERT_PTR_NOT_NULL(n2);
	CU_ASSERT(n3 == push_tail(n3));
	CU_ASSERT_PTR_NOT_NULL(n3);
	CU_ASSERT(3 == list_size());
	CU_ASSERT(n1 == get_pos(1));
	CU_ASSERT_PTR_NOT_NULL(n1);
	CU_ASSERT(n2 == get_pos(2));
	CU_ASSERT_PTR_NOT_NULL(n2);
	CU_ASSERT(n3 == get_pos(3));
	CU_ASSERT_PTR_NOT_NULL(n3);
	CU_ASSERT_PTR_NULL(get_pos(list_size() + 1));
	if (list_size() > 0) {
		CU_ASSERT(i3 == *(int *)(pop_tail()));
		CU_ASSERT(i2 == *(int *)(pop_tail()));
		CU_ASSERT(i1 == *(int *)(pop_tail()));
	}
	CU_ASSERT(0 == list_size());
	CU_ASSERT_PTR_NULL(get_pos(0));
	CU_ASSERT_PTR_NULL(get_pos(-1));
	CU_ASSERT_PTR_NULL(get_pos(list_size() + 1));
}
