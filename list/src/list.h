/* 
 * Module      : ZHAW PROGC list
 * Description : float linked list
 * $Id: list.h 2015-07-02 metl$
 */

#ifndef LIST_H_
#define LIST_H_

#ifndef FALSE
#define FALSE 		0
#define TRUE 		!FALSE
#endif /* FALSE */

typedef struct Node {
 void *e;
 struct Node *next;
 struct Node *prev;
} Node;

Node *new_node(void *);
Node *push_head(Node *);
Node *push_tail(Node *);
void *pop_head(void);
void *pop_tail(void);
Node *insert_after(Node *, Node *);
Node *insert_before(Node *, Node *);
void erase_node(Node *);
Node *get_pos(int);
int list_size();
void print_list();
int check_consistency (Node *);

#endif /* LIST_H_ */
