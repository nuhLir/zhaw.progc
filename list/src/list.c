/* 
 * Module      : ZHAW PROGC Node
 * Description : float linked Node
 * $Id: Node.c 2015-07-02 metl$
 */
 
#include <stdlib.h>
#include <stdio.h>
#include "list.h"
 
Node *head = NULL;
Node *tail = NULL;

/* 
 * allocate and return a new Node
 * params:
 *	void * > (void) pointer to Element
 * return:
 *	Node * > pointer to new allocated Node with
 *           - void *element assigned to Element
 *           - *next and *prev initialized with NULL
 *           or
 *           - NULL when Element is NULL
 *           - NULL when allocation fails
 */
Node *new_node(void *e) {
	if (e == NULL) return NULL;

	Node *node = (Node *)malloc(sizeof(Node));

	if (node == NULL) return NULL;

	node->e = e;
	node->prev = NULL;
	node->next = NULL;

	/* to do */
    return node;
}

/* 
 * push a new Node at the head of List
 * params:
 *	void * > pointer to new Node
 * return:
 *	Node * > pointer to new head, or
 *           - NULL when Node is NULL
 *           - NULL when Element is NULL
 *           - NULL when Node is already in List
 */
Node *push_head (Node *new) {
	if (new == NULL || new->e == NULL) return NULL;

	if (head != NULL) {
		Node *current = head;

		while(current != NULL){
			if (current == new) return NULL;

			current = current->prev;
		}

		head->next = new;
		new->prev = head;
	}

	head = new;

    return head;
}

/* 
 * push a new Node at the tail of List
 * params:
 *	void * > pointer to new Node
 * return:
 *	Node * > pointer to new tail, or
 *           - NULL when Node is NULL
 *           - NULL when Element is NULL
 *           - NULL when Node is already in List
 */
Node *push_tail (Node *new) {
    /* to do */
    return NULL;
}

/* 
 * pop and free Node from head of List
 * params:
 *	void 
 * return:
 *	void * > pointer to Element (and free popped Node), or
 *           - NULL on empty List
 */
void *pop_head(void) {
	

    /* to do */
    return NULL;
}

/* 
 * pop anf free Node from tail of List
 * params:
 *	void 
 * return:
 *	void * > pointer to Element (and free popped Node), or
 *           - NULL on empty List 
 */
void *pop_tail(void) {
    /* to do */
    return NULL;
}

/* 
 * insert Node after Node
 * params:
 *	Node * > pos: pointer to actual Node
 *	Node * > n: pointer to Node to be inserted
 * return:
 *	Node * > pointer to inserted Node, or
 *           - NULL when Node to be inserted is NULL
 *           - push Node at end of List when actual Node is NULL
 */
Node *insert_after(Node *pos, Node *n) {
    /* to do */
    return NULL;
}

/* 
 * insert Node before Node
 * params:
 *	Node * > pos: pointer to actual Node
 *	Node * > n: pointer to Node to be inserted
 * return:
 *	Node * > pointer to inserted Node, or 
 *           - NULL when Node to be inserted is NULL
 *           - push Node at start of List when actual Node is NULL
 */
Node *insert_before(Node *pos, Node *n) {
    /* to do */
    return NULL;
}

/* 
 * free allocated Node and its Element
 * params:
 *	Node * > pointer to Node
 * return:
 *	void > when Node is NULL otherwise
 *         - free allocated (List) Element
 *         - free allocated Node
 */
void erase_node(Node *n) {
    /* to do */
}

/* 
 * get Node at defined position
 * params:
 *	int > position in List
 * return:
 *	Node * > pointer to Node, or
 *			 - NULL on empty list
 *           - NULL when pos < 1
 *           - NULL when pos > List size
 */
Node *get_pos(int pos){ 
	/* to do */
    return NULL;
}

/* 
 * get List size
 * params:
 *	void
 * return:
 *	int > size of List
 */
int list_size() {
	if (head == NULL) return 0;

	int node_size = 1;
	Node *current = head;

	while(current != NULL){
		node_size ++;
		current = current->prev;
	}


    /* to do */
    return node_size;
}

/* 
 * print all List Elements
 * params:
 *	void
 * return:
 *	void
 */
void print_list(){
    /* to do */
}

/* 
 * check consistency of List
 * params:
 *	Node * > pointer to Node to be ckecked
 * return:
 *	int > TRUE when Node not in List, FALSE otherwise
 */
int check_consistency (Node *c) {
	/* to do */
	return FALSE;
}
	
